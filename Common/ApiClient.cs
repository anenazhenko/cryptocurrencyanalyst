﻿using ApiClient;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Common
{
    public class ApiClient : IService
    {
        private HttpClient HttpClient { get; set; }

        public ApiClient()
        {
            CreateHttpClient();
        }

        public void SetBaseUrl(string url)
        {
            HttpClient.BaseAddress = new Uri(url);
        }

        public void SetHeader(string name, string value)
        {
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation(name, value);
        }

        private void CreateHttpClient()
        {
            HttpClient = new HttpClient();
        }

        public async Task<TReturn> GetAsync<TReturn>(string url, CancellationToken ct)
        {
            var response = await HttpClient.GetAsync(url, ct);
            CheckStatusCode(response);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TReturn>(responseContent);
        }

        private void CheckStatusCode(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new ApiException(response);
            }
        }
    }
}
