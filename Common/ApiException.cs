﻿using System;
using System.Net.Http;

namespace ApiClient
{
    public class ApiException : Exception
    {
        public HttpResponseMessage Response { get; set; }

        public ApiException(HttpResponseMessage response)
        {
            Response = response;
        }
    }
}