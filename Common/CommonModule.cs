﻿using Autofac;
using System.Linq;
using System.Reflection;

namespace Common
{
    public class CommonModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(GetType().GetTypeInfo().Assembly)
                .Where(t => t.GetTypeInfo().ImplementedInterfaces.Intersect(new[] { typeof(IService) }).Any())
                .AsImplementedInterfaces()
                .AsSelf()
                .InstancePerDependency();
        }
    }
}
