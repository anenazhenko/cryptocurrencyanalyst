﻿using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    public class CoinMarketCapResponseModel<TData>
    {
        [DataMember(Name = "status")]
        public CoinMarketCapStatusModel Status { get; set; }

        [DataMember(Name = "data")]
        public TData Data { get; set; }
    }
}
