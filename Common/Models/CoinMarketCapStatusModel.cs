﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    public class CoinMarketCapStatusModel
    {
        [DataMember(Name = "timestamp")]
        public DateTime RequestDate { get; set; }

        [DataMember(Name = "pretty_date")]
        public string PrettyDate => RequestDate.ToLocalTime().ToString("F", CultureInfo.GetCultureInfo("en-US"));

        [DataMember(Name = "error_code")]
        public int ErrorCode { get; set; }

        [DataMember(Name = "error_message")]
        public string ErrorMessage { get; set; }

        [DataMember(Name = "elapsed")]
        public int Elapsed { get; set; }

        [DataMember(Name = "credit_count")]
        public int CreditCount { get; set; }
    }
}
