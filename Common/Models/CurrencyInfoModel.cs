﻿using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    public class CurrencyInfoModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "symbol")]
        public string Symbol { get; set; }

        [DataMember(Name = "quote")]
        public QuoteModel Quote { get; set; }

        [DataMember(Name = "in_favorites")]
        public bool InFavorites = false;
    }
}