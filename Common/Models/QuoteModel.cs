﻿using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    public class QuoteModel
    {
        [DataMember(Name = "USD")]
        public UsdQuoteModel UsdQuote { get; set; }
    }
}