﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    public class UsdQuoteModel
    {
        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "volume_24h")]
        public double Volume24hours { get; set; }

        [DataMember(Name = "percent_change_1h")]
        public double PercentChange1hour { get; set; }

        [DataMember(Name = "percent_change_24h")]
        public double PercentChange24hours { get; set; }

        [DataMember(Name = "percent_change_7d")]
        public double PercentChange7days { get; set; }

        [DataMember(Name = "market_cap")]
        public double MarketCap { get; set; }

        [DataMember(Name = "last_updated")]
        public DateTime LastUpdated { get; set; }   
    }
}