﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Configurations;
using Common.Models;
using Microsoft.Extensions.Options;

namespace Common.Services
{
    public class CoinMarketCapService : IService
    {
        private readonly ApiClient _apiClient;

        public CoinMarketCapService(ApiClient apiClient, IOptions<CoinMarketCapConfiguration> configuration)
        {
            _apiClient = apiClient;
            _apiClient.SetBaseUrl("https://pro-api.coinmarketcap.com");
            _apiClient.SetHeader("Accept", "application/json");
            _apiClient.SetHeader("X-CMC_PRO_API_KEY", configuration.Value.ApiKey);
        }

        public async Task<CoinMarketCapResponseModel<List<CurrencyInfoModel>>> GetCurrenciesInfo(List<int> favoriteCurrencies, CancellationToken ct)
        {
            var currenciesInfo = await _apiClient
                .GetAsync<CoinMarketCapResponseModel<List<CurrencyInfoModel>>>("/v1/cryptocurrency/listings/latest?limit=30",
                    ct);

            currenciesInfo.Data = currenciesInfo.Data
                .OrderByDescending(x => x.Quote.UsdQuote.MarketCap)
                .ToList();

            var favoriteCurrenciesinfos = currenciesInfo.Data
                .Where(x => favoriteCurrencies
                    .Contains(x.Id))
                .Select(x =>
                {
                    x.InFavorites = true;
                    return x;
                })
                .OrderByDescending(x => x.Quote.UsdQuote.MarketCap);

            currenciesInfo.Data = favoriteCurrenciesinfos
                .Union(currenciesInfo.Data)
                .ToList();

            return currenciesInfo;
        }
    }
}
