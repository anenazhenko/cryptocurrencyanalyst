﻿using System;
using DataAccess;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Common.Services
{
    public class FavoritesService : IService
    {
        private readonly IContextManager<DataContext> _contextManager;

        public FavoritesService(IContextManager<DataContext> contextManager)
        {
            _contextManager = contextManager;
        }

        public List<int> ParseFavoritesCookie(string cookieValue)
        {
            var parseResult = new List<int>();

            if (string.IsNullOrWhiteSpace(cookieValue))
                return parseResult;

            parseResult.AddRange(cookieValue
                .Split(",")
                .Select(x => int.Parse(x))
                .Distinct());

            return parseResult;
        }

        public CookieOptions GetFavCookieOptions()
        {
            return new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                IsEssential = true,
                Expires = DateTimeOffset.MaxValue,
            };
        }

        public async Task<List<int>> GetUserFavorites(User user, CancellationToken ct)
        {
            using (var db = _contextManager.CreateContext())
            {
                var userFavoriteCurrencies = await db.FavoriteCurrencies
                    .Where(x => x.User.Id == user.Id)
                    .Select(x => x.CurrencyId)
                    .ToListAsync(ct);

                return userFavoriteCurrencies;
            }
        }

        public async Task AddUserFavoriteAsync(User user, int favId, CancellationToken ct)
        {
            using (var db = _contextManager.CreateContext())
            {
                var userInContext = await db.Users
                    .FirstOrDefaultAsync(x => x.Id == user.Id);

                var isCurrencyAlreadyInFav =
                    db.FavoriteCurrencies
                        .Any(x => x.User.Id == user.Id && x.CurrencyId == favId);

                if (!isCurrencyAlreadyInFav)
                {
                    db.FavoriteCurrencies.Add(new FavoriteCurrency
                    {
                        User = userInContext,
                        CurrencyId = favId
                    });

                    await db.SaveChangesAsync(ct);
                }
            }
        }

        public async Task RemoveUserFavoriteAsync(User user, int favId, CancellationToken ct)
        {
            using (var db = _contextManager.CreateContext())
            {
                var favToRemove = await db.FavoriteCurrencies
                    .FirstOrDefaultAsync(x => x.User.Id == user.Id && x.CurrencyId == favId);

                if (favToRemove != null)
                {
                    db.Remove(favToRemove);

                    await db.SaveChangesAsync(ct);
                }
            }
        }
    }
}
