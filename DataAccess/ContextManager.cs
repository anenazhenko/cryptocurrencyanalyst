﻿using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class ContextManager : IContextManager<DataContext>
    {
        private readonly DataContext _context;
        public string ConnectionString { get; }

        public ContextManager(string connectionString)
        {
            ConnectionString = connectionString;
            _context = CreateContext();
        }

        public DataContext GetContext()
        {
            return _context;
        }

        public DataContext CreateContext()
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseNpgsql(ConnectionString);

            var options = builder.Options;

            return new DataContext(options);
        }
    }
}
