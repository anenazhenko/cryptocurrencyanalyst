﻿using Autofac;

namespace DataAccess
{
    public class DataAccessModule : Module
    {
        private readonly string _connectionString;

        public DataAccessModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(DataContext))
                .As<DataContext>();

            builder.Register(x => new ContextManager(_connectionString))
                .As<IContextManager<DataContext>>()
                .InstancePerLifetimeScope();
        }
    }
}
