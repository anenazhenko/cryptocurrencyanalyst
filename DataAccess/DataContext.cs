﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class DataContext : IdentityDbContext<User>
    {
        public DbSet<FavoriteCurrency> FavoriteCurrencies { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
        }
    }
}
