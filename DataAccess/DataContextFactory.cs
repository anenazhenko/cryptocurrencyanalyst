﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccess
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        private const string ConnectionStringName = "DefaultConnection";

        private DataContext Create(string basePath)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            var config = builder.Build();

            var connectionString = config.GetConnectionString(ConnectionStringName);

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new InvalidOperationException(
                    $"Could not find a connection string named '{ConnectionStringName}'.");
            }

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException(
                    $"{nameof(connectionString)} is null or empty.",
                    nameof(connectionString));

            var optionsBuilder =
                new DbContextOptionsBuilder<DataContext>();

            optionsBuilder.UseNpgsql((string)connectionString);

            return new DataContext(optionsBuilder.Options);
        }

        public DataContext CreateDbContext(string[] args)
        {
            var projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { @"bin\" }, StringSplitOptions.None)[0];
            return Create(projectPath);
        }
    }
}