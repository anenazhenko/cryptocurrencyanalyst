﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccess
{
    public static class DataRegistration
    {
        public static IServiceCollection UseDatabase(this IServiceCollection services, string sqlConnectionString)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseNpgsql(
                    sqlConnectionString,
                    b => b.MigrationsAssembly("DataAccess")
                )
            );

            return services;
        }
    }
}
