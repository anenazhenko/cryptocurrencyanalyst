﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class FavoriteCurrency
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public User User { get; set; }

        public int CurrencyId { get; set; }
    }
}
