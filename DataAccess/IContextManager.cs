﻿using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public interface IContextManager<out TDbContext> where TDbContext : DbContext
    {
        TDbContext CreateContext();
    }
}
