﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DataAccess
{
    public static class Seeder
    {
        public static void Migrate(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<DataContext>();

            dbContext.Database.Migrate();
        }
    }
}
