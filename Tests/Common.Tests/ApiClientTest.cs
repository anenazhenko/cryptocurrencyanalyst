using ApiClient;
using System;
using System.Threading;
using Xunit;

namespace Common.Tests
{
    public class ApiClientTest
    {
        [Fact]
        public void BadUriTest()
        {
            var apiClient = new ApiClient();

            try
            {
                apiClient.GetAsync<string>("/test", CancellationToken.None).Wait();
            }
            catch (AggregateException exception)
            {
                Assert.IsType<InvalidOperationException>(exception.InnerException);

                Assert.Equal("An invalid request URI was provided. The request URI must either be an absolute URI or BaseAddress must be set.",
                    exception.InnerException.Message,
                    StringComparer.OrdinalIgnoreCase);
            }
        }

        [Fact]
        public void ApiExceptionTest()
        {
            var apiClient = new ApiClient();

            apiClient.SetBaseUrl("https://www.google.ru");

            try
            {
                apiClient.GetAsync<string>("/test", CancellationToken.None).Wait();
            }
            catch (AggregateException exception)
            {
                Assert.IsType<ApiException>(exception.InnerException);
            }
        }
    }
}
