﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Controllers
{
    [Route("account/")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        [Route("register/")]
        public IActionResult RegisterGet()
        {
            return View("Register");
        }

        [HttpPost]
        [Route("register/")]
        public async Task<IActionResult> Register([FromForm]CredentialsModel credentials)
        {
            await _userManager.CreateAsync(new User { UserName = credentials.Login }, credentials.Password);

            return RedirectToAction("LoginGet", "Account");
        }

        [HttpGet]
        [Route("login/")]
        public IActionResult LoginGet()
        {
            return View("Login");
        }

        [HttpPost]
        [Route("login/")]
        public async Task<IActionResult> Login([FromForm]CredentialsModel credentials, [FromForm]bool rememberMe = false)
        {
            var user = await _userManager.FindByNameAsync(credentials.Login);

            if (user != null)
            {
                var logInResult = await _signInManager.PasswordSignInAsync(user, credentials.Password, rememberMe, false);

                if (!logInResult.Succeeded)
                {
                    return LoginGet();
                }
            }
            else
            {
                return LoginGet();
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [Route("logout/")]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}