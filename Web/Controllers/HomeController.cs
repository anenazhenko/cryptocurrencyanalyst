﻿using System.Collections.Generic;
using Common.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly CoinMarketCapService _coinMarketCapService;
        private readonly FavoritesService _favoritesService;
        private readonly UserManager<User> _userManager;

        public HomeController(CoinMarketCapService coinMarketCapService,
            FavoritesService favoritesService,
            UserManager<User> userManager)
        {
            _coinMarketCapService = coinMarketCapService;
            _favoritesService = favoritesService;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> CurrenciesInfo(CancellationToken ct)
        {
            var favorites = new List<int>();
            if (!User.Identity.IsAuthenticated)
            {
                Request.Cookies.TryGetValue("fav_currencies", out string favoritesCookie);
                favorites = _favoritesService.ParseFavoritesCookie(favoritesCookie);
            }
            else
            {
                var user = await _userManager.GetUserAsync(User);
                favorites = await _favoritesService.GetUserFavorites(user, ct);
            }

            var currenciesInfo = await _coinMarketCapService.GetCurrenciesInfo(favorites, ct);

            return Json(currenciesInfo);
        }

        public async Task<IActionResult> AddFavorite(int id, CancellationToken ct)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Request.Cookies.TryGetValue("fav_currencies", out string favoritesCookie);

                var favorites = _favoritesService.ParseFavoritesCookie(favoritesCookie);
                if (!favorites.Any(x => x == id))
                {
                    favorites.Add(id);
                }

                Response.Cookies.Append("fav_currencies", string.Join(',', favorites), _favoritesService.GetFavCookieOptions());
            }
            else
            {
                var user = await _userManager.GetUserAsync(User);
                await _favoritesService.AddUserFavoriteAsync(user, id, ct);
            }

            return Ok();
        }

        public async Task<IActionResult> RemoveFavorite(int id, CancellationToken ct)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Request.Cookies.TryGetValue("fav_currencies", out string favoritesCookie);

                var favorites = _favoritesService.ParseFavoritesCookie(favoritesCookie);
                if (favorites.Any(x => x == id))
                {
                    favorites.Remove(id);
                }

                Response.Cookies.Append("fav_currencies", string.Join(',', favorites), _favoritesService.GetFavCookieOptions());
            }
            else
            {
                var user = await _userManager.GetUserAsync(User);
                await _favoritesService.RemoveUserFavoriteAsync(user, id, ct);
            }

            return Ok();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}