﻿namespace Web.Models
{
    public class CredentialsModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
