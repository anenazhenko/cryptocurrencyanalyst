﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = "http://0.0.0.0:55000";
            if (args.Length > 0)
            {
                host = args[0];
            }

            BuildWebHost(host).Run();
        }

        public static IWebHost BuildWebHost(string host) =>
            WebHost.CreateDefaultBuilder()
                .UseUrls(host)
                .UseStartup<Startup>()
                .Build();
    }
}
