﻿$(document).ready(function () {
    var dataTable;
    if (!dataTable) {
        dataTable = new DataTable();
    }
});

function showAlert(isSuccess) {
    if (isSuccess) {
        var successAlert = document.getElementById('alertSuccess');
        successAlert.hidden = false;
        setTimeout(function () {
            successAlert.hidden = true;
        }, 2500);
    } else {
        var dangerAlert = document.getElementById('alertDanger');
        dangerAlert.hidden = false;
        setTimeout(function () {
            dangerAlert.hidden = true;
        }, 2500);
    }
}

function DataTable() {
    var context = this;

    this.state = {
        currenciesInfo: {}
    };

    getData();

    setInterval(function () {
        getData();
    }, 60000);

    function getData() {
        var currenciesInfoPromise = new Promise(getCurrenciesInfo);

        Promise.all([
            currenciesInfoPromise
        ]).then(() => {
            renderTable(true);
        }).catch(() => {
            renderTable(false);
        });
    }

    function getCurrenciesInfo(resolve, reject) {
        $.ajax({
            url: '/home/currenciesinfo',
            method: 'GET',
            success: function (data) {
                context.state.currenciesInfo = data;
                resolve();
            },
            error: function () {
                reject();
            }
        });
    }

    function addToFav(id) {
        var currenciesArray = context.state.currenciesInfo.data;
        var element;
        for (var i = 0; i < currenciesArray.length; i++) {
            element = currenciesArray[i];
            if (element.id === id) {
                element.in_favorites = true;
                currenciesArray.splice(i, 1);
                currenciesArray.splice(0, 0, element);
                break;
            }
        }
    }

    function removeFromFav(id) {
        var currenciesArray = context.state.currenciesInfo.data;
        var element;
        for (var i = 0; i < currenciesArray.length; i++) {
            element = currenciesArray[i];
            if (element.id === id) {
                element.in_favorites = false;
                currenciesArray.splice(i, 1);
                currenciesArray.splice(currenciesArray.length, 0, element);
                break;
            }
        }
    }

    function renderTable(isJSON) {
        var data = context.state;
        var tableTemplate;
        if (isJSON) {
            tableTemplate = $.templates('#tableTemplate');
            $('#currenciesInfoContainer').html(tableTemplate.render(data));
            $('.favButtonAdd').on('click',
                function () {
                    var currencyId = parseInt($(this).attr('data-id'));
                    $.ajax({
                        url: '/home/addfavorite',
                        method: 'POST',
                        data: { id: currencyId},
                        success: function () {
                            showAlert(true);
                            addToFav(currencyId);
                            renderTable(true);
                        },
                        error: function () {
                            showAlert(false);
                        }
                    });
                });
            $('.favButtonRemove').on('click',
                function () {
                    var currencyId = parseInt($(this).attr('data-id'));
                    $.ajax({
                        url: '/home/removefavorite',
                        method: 'POST',
                        data: { id: currencyId },
                        success: function () {
                            showAlert(true);
                            removeFromFav(currencyId);
                            renderTable(true);
                        },
                        error: function () {
                            showAlert(false);
                        }
                    });
                });
            $('.filter').on('change',
                function () {
                    var currenciesString = this.value.toLowerCase();
                    var currencies = currenciesString.split(',');
                    currencies = currencies.map(currency => currency.trim());
                    var rows = $('.currencyTableRow');
                    if (currenciesString === '') {
                        for (let row of rows) {
                            row.hidden = false;
                        }
                    } else {
                        for (let row of rows) {
                            var nameCell = row.querySelector('.nameCell');
                            var name = nameCell.innerText.trim().toLowerCase();
                            if (currencies.indexOf(name) < 0) {
                                row.hidden = true;
                            } else {
                                row.hidden = false;
                            }
                        }
                    }
                });
        } else {
            tableTemplate = $.templates('#noData');
            $('#currenciesInfoContainer').html(tableTemplate.render(data));
        }
    }
}